from urllib2 import urlopen
from pymongo import Connection
from random import randrange
import time
import json
import logging


connection = Connection()
db = connection.fb_users
num_list = []
content = []
logging.basicConfig(filename='example.log')

for i in range(1, 100000):
    try:
        id = randrange(100000000, 6000000000)
        if id not in num_list:
            num_list.append(id)
            content = json.load(urlopen("https://graph.facebook.com/%s" % (id)))
            if not content:
                logging.warning("No Profile %s" % (id))
                continue
            else:
                acc = db.fb_users
                acc.insert(content)
                logging.info("Added new user %s [%s]" % (content['name'], id))
                time.sleep(0.1)
        else:
            logging.warning("Repeat %s" % (id))
            continue
    except:

        logging.error("Exception %s" % (id))
        continue
