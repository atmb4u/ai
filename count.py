import re
import linecache
i = 1##########This Number will get replaced
import count
def main():
    """
    This program modifies its own code (just a bit) and reloads itself .
    It then runs itself, which makes it a n evolving program in some sense
    This is a simple demonstration of how self modifying code can be
    implemented in python
    """
    c = linecache.getline("count.py",3)
    m = re.findall('\d+',c)
    f = open("count.py", "r+")
    f.seek(31)
    f.write(str(int(m[0])+1))
    f.close()
    print i

if __name__ == '__main__':
    reload(count) # reloads the  module
    main()
