from random import randrange
import logging
import time
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

mpl.rcParams['legend.fontsize'] = 10
ENERGY = 600
distance =[]

def radiation(food, x1, y1, z1):
    """
    The radiation function
    Generates smell of the food depending on the energy of the food and distance
    of the user from the food.
    Input:
    food - Food object
    x1, y1, z2 - Current position of the player
    Output:
    Axis - Which axis the distance is (X, Y, Z)
    Radiation Level - Intensity of smell
    """
    radiation = 0
    x2 = 0
    y2 = 0
    z2 = 0
    if  abs(food.food_x-x1)<food.energy/10:
        x2 = abs(food.food_x-x1)
    else:
        x2 = 0
    if  abs(food.food_y-y1)<food.energy/10:
        y2 = abs(food.food_y-y1)
    else:
        y2 = 0
    if  abs(food.food_z-z1)<food.energy/10:
        z2 = abs(food.food_z-z1)
    else:
        z2 = 0
    """
    For traversing the least distance available towards the food
    """
    distance.append(((food.food_x - x1) ** 2 + (food.food_y - y1) ** 2 + (food.food_z - z1) ** 2) ** 0.5)
    d1 = d2 = d3 = 100000.0
    if food.food_x - x1 >0:
        d1 = ((food.food_x - x1 - 1) ** 2 + (food.food_y - y1) ** 2 + (food.food_z - z1) ** 2) ** 0.5
    else:
        d1 = ((food.food_x - x1 + 1) ** 2 + (food.food_y - y1) ** 2 + (food.food_z - z1) ** 2)** 0.5
    if food.food_y-y1 > 0:
        d2 = ((food.food_x - x1) ** 2 + (food.food_y - y1 - 1) ** 2 + (food.food_z - z1) ** 2) ** 0.5
    else:
        d2 = ((food.food_x - x1) ** 2 + (food.food_y - y1 + 1) ** 2 + (food.food_z - z1) ** 2) ** 0.5
    if food.food_z-z1 > 0:
        d3 = ((food.food_x-x1) ** 2 + (food.food_y-y1) ** 2 + (food.food_z - z1 - 1) ** 2) ** 0.5
    else:
        d3 = ((food.food_x-x1) ** 2 + (food.food_y-y1) ** 2 + (food.food_z - z1 + 1) ** 2) ** 0.5
    radiate= max(d1, d2, d3)
    if radiate != 100000.0:
        if d1 <= d2 and d1 <= d3 and abs(food.food_x-x1) <= food.energy/10 and abs(food.food_x-x1) != 0:
            distance.append(d1)
            return "X", food.food_x-x1, distance
        if d2 <= d1 and d2 <= d3 and abs(food.food_y-y1) <= food.energy/10 and abs(food.food_y-y1) != 0:
            distance.append(d2)
            return "Y", food.food_y-y1, distance
        if d3 <= d1 and d3 <= d2 and abs(food.food_z-z1) <= food.energy/10 and abs(food.food_z-z1) != 0:
            distance.append(d3)
            return "Z", food.food_z-z1, distance
    return "", 0, distance

class Food():
    def __init__(self):
        self.energy = randrange(0, 1000) #Chances of continuing in the game is tightly depended on this
        self.food_x = randrange(0, 100)
        self.food_y = randrange(0, 100)
        self.food_z = randrange(0, 100)
def main():
    """
    The main function of the game.
    """    
    x_max = False #Flag for  reaching maximum in x- axis
    y_max = False
    z_max = False
    last_move = '' # Flag for last axis of movement
    total_food = 0
    x_array = [] #Lists for matplotlib purposes
    y_array = []
    z_array = []
    start =[randrange(0, 100), randrange(0, 100), randrange(0, 100)] # starting point is random
    current = start
    energy = ENERGY
    food = Food()
    while energy: # Checking if the energy of the player is ZERO
        axis, rad, distance = radiation(food, current[0], current[1], current[2])
        if axis:
            try:
                """
                Depending on the distance, steer the player towards the food.
                Use axis, radiation provided from radiation function
                """
                if distance[-2]<distance[-1]:
                    print "Direction reversal %s" % (last_move)
                    if last_move == 'X':
                        if x_max:
                            x_max = False
                        else:
                            x_max = True
                    if last_move == 'Y':
                        if y_max:
                            y_max = False
                        else:
                            y_max = True
                    if last_move == 'Z':
                        if z_max:
                            z_max = False
                        else:
                            z_max = True
            except:
                "Initial data error, This is normal"
            last_move = axis
            if axis == 'X' and current[0] != food.food_x:
                last_move ='X'
                if rad >0 and current[0] < 100:
                    current[0]+=1
                    energy-=1
                elif current[0] > 0:
                    current[0]-=1
                    energy-=1
            if axis == 'Y' and current[1] != food.food_y:
                if rad >0 and current[1] < 100:
                    current[1]+=1
                    energy-=1
                elif current[1] > 0:
                    current[1]-=1
                    energy-=1
            if axis == 'Z'  and  current[2] != food.food_z:
                if rad > 0 and current[2] < 100:
                    current[2]+=1
                    energy-=1
                elif current[2] > 0:
                    current[2]-=1
                    energy-=1
        else:
            """
            If no radiation dectected, uses random walk
            Reversal of direction if limit reached
            """
            ran = randrange(1, 4)            
            if ran ==1 or (food.food_x != current[0] and \
                           distance[-1] < food.energy/10):
                last_move = 'X'
                if current[0] == 100:
                    x_max = True
                if current[0] == 0:
                    x_max = False
                if x_max:
                    current[0]-=1
                else:
                    current[0]+=1
                energy-=1
            if ran ==2 or (food.food_y != current[1] and \
                           distance[-1] < food.energy/10):
                last_move = 'Y'
                if current[1] == 100:
                    y_max = True
                if current[1] == 0:
                    y_max = False
                if y_max:
                    current[1]-=1
                else:
                    current[1]+=1
                energy-=1
            if ran ==3 or (food.food_z != current[2] and \
                           distance[-1] < food.energy/10):
                last_move = 'Z'
                if current[2] == 100:
                    z_max = True
                if current[2] == 0:
                    z_max = False
                if z_max:
                    current[2]-=1
                else:
                    current[2]+=1
                energy-=1
        x_array.append(current[0])
        y_array.append(current[1])
        z_array.append(current[2])
        try:
            print "Estimated Distance: ",distance[-1], "\nCurrent Location:", current, "\n[", energy, "] left"
        except:
            pass
        time.sleep(0.1)
        if food.energy == 0:
            print "You made me wander through arid desert, and now I'm dead."
        if food.food_x == current[0] and food.food_y == current[1] and food.food_z == current[2]:
            print "Had food!!! Yummy!!!\n ::::::Status::::::"
            total_food = total_food + food.energy
            print "Initial   Energy  : %s" % (ENERGY)
            print "Spend     Energy* : %s" % (ENERGY+total_food - energy)
            print "Remaining Energy  : %s" % (energy)
            print "Food      Energy  : %s" % (food.energy)
            print "Total     Energy  : %s" % (food.energy + energy)
            energy = food.energy + energy
            fig = plt.figure()         
            ax1 = fig.add_subplot(111, projection='3d')
            ax1.scatter([food.food_x,], [food.food_y,], food.food_z, c='g', marker='o')
            ax1.scatter([start[0],], [start[1],], start[2], c='g', marker='o')
            ax2 = fig.gca(projection='3d')
            ax2.plot(x_array, y_array, z_array, label='anteater')
            ax2.legend()            
            plt.show()
            a =raw_input("Do you want me to hunt again (Y/n)?")
            if a =="n":
                b =raw_input("Do you want me to save the trace (Y/n)?")
                if b == "y" or b == "Y":
                    f = open("trace.log","w")
                    f.write("{ xarray : %s, yarray : %s zarray : %s}" % (x_array, y_array, z_array))
                    print "Saved as trace.log"
                break
            else:
                start = current
                food = Food()
                
if __name__ == "__main__":
    main()

