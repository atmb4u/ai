from pymongo import Connection
import pylab


connection = Connection()
db = connection.fb_users
acc = db.fb_users
cur = acc.find()
gcount = []
bcount = []
number = []
delta = []
NB = 0
NG = 0
prob1 = 0
prob2 = 0
prob3 = 0
for counter, value in enumerate(cur):
    if 'gender' in value:
        number.append(counter)
        if value['gender'] == 'male':
            NB += 1
        else:
            NG += 1
        prob1 = NB / 613.0
        prob2 = NG / 613.0
        prob3 = (NB - NG) / 613.0
        bcount.append(prob1)
        gcount.append(prob2)
        delta.append(prob3)
pylab.plot(number, gcount, 'r')
pylab.plot(number, bcount, 'b')
pylab.plot(number, delta, 'g')
pylab.savefig('fb_gender_data.png')
